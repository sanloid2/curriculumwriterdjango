from django.shortcuts import render
from django.http import HttpResponse


def index(request):
    return render(request, "main/main.html")

def about(request):
    return render(request, "main/about.html")

def contacts(request):
    return render(request, "main/contacts.html")

def other(request):
    return render(request, "main/other.html")

